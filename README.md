# Data Engineering Python Test


### Executando o código
Jupyter Notebook disponível no arquivo ```Data Engineering Python Test.ipynb``` na raiz do projeto 

### Definindo dados de acesso
 - No arquivo [.env](.env) na raiz do projeto, colocar as chaves pública e privadas(apenas o número, sem aspas)

### Instalando dependências do projeto
```
pip install python-decouple
pip install pandas
```


### Executando testes
Na raiz do projeto execute:
```
pytest
```
