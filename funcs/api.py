import time, calendar, os, hashlib, requests
from requests.exceptions import HTTPError
from pathlib import Path
from decouple import config

def get_apikey():
    """
    A partir de um par de chaves gera a chave de conexão com a API

    Retorno:
    {
        - error <bool>
        - tz <str> 
        - apikey <str>
    }
    """
    
    root_project = Path(os.getcwd())
    have_dot_env = os.path.isfile(os.path.join(root_project, '.env'))

    data_returned = {'error' : True, 'tz' : '', 'apikey' : ''}
    
    if not have_dot_env:
        data_returned['error'] = True
        return data_returned

    try:
        public_key = config('PUBLIC_KEY')
        private_key = config('PRIVATE_KEY')

        gmt = time.gmtime()
        ts = calendar.timegm(gmt)

        str = f'{ts}{private_key}{public_key}'
        hstr = hashlib.md5(str.encode())
        key = hstr.hexdigest()  

    except e:
        data_returned['error'] = True
        return data_returned

    data_returned['error'] = False
    data_returned['tz'] = f'{ts}'
    data_returned['apikey'] = key

    return data_returned
       


def get_data(**kwargs):
    """
    Busca dados de personagens na API
    Argumentos:
        - skip <int> 
        - limit <int> 
        - base_url <str>
        - apikey <str>
        - ts <int>
    
    Retorno:
        {
            - error <bool>
            - data <list> | <str>
        }
    """

    data_returned = {'error' : True, 'data' : ''}

    try:
        base_url = kwargs['base_url']
        skip = kwargs['skip']
        limit = kwargs['limit']
        apikey = kwargs['apikey']
        ts = kwargs['ts']
        public_key = config('PUBLIC_KEY')
        
        valid = isinstance(skip, int)
        valid = isinstance(base_url, str)
        valid = isinstance(limit, int)
        valid = isinstance(apikey, str)
        valid = isinstance(ts, str)       

    except:
        return data_returned

    url = f'{base_url}?ts={ts}&apikey={public_key}&hash={apikey}&limit={limit}&offset={skip}'
    
    try:
        response = requests.get(url)
        response.raise_for_status()
    except HTTPError as http_error:
        data_returned['data'] = http_error
        return data_returned
    except Exception as error:
        data_returned['data'] = error
        return data_returned
    else:
        data_returned['error'] = False
        data_returned['data'] = response.json()
        return data_returned
    