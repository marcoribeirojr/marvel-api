from funcs import api
import os


def test_return_false_if_not_exists_a_config_file():
    data_returned = api.get_apikey()
    assert data_returned['error'] == True

def test_return_False_if_not_have_public_and_private_keys():
    filename = '.env'
    with open(filename, 'w') as f:
        line = f'PUBLIC_KEY=abc'        
        f.write(line)    
    data_returned = api.get_apikey()
    os.remove(filename)

    assert data_returned['error'] == True   

def test_having_public_and_private_keys_return_the_key():
    filename = '.env'
    with open(filename, 'w') as f:
        line = f'PUBLIC_KEY=abc\nPRIVATE_KEY=defgh'        
        f.write(line)    
    data_returned = api.get_apikey()
    os.remove(filename)

    valid_return = (isinstance(data_returned['tz'], str) == True) and (isinstance(data_returned['apikey'], str) == True) 

    assert valid_return == True 

def test_all_arguments_must_be_passed():
    filename = '.env'
    with open(filename, 'w') as f:
        line = f'PUBLIC_KEY=abc\nPRIVATE_KEY=defgh'        
        f.write(line) 

    data_returned = api.get_data( 
        skip = 0,
        base_url = '',
        apikey = '',
        ts = ''
    ) 
    os.remove(filename)
    assert data_returned['error'] == True

def test_not_accept_invalid_arguments():

    filename = '.env'
    with open(filename, 'w') as f:
        line = f'PUBLIC_KEY=abc\nPRIVATE_KEY=defgh'        
        f.write(line)    
   
    data_returned = api.get_data( 
        skip = 0,
        base_url = '',
        limit = '',
        api = '',
        ts = ''
    )     

    os.remove(filename)
    assert data_returned['error'] == True

def test_error_with_invalid_types():
    
    filename = '.env'
    with open(filename, 'w') as f:
        line = f'PUBLIC_KEY=abc\nPRIVATE_KEY=defgh'        
        f.write(line)    
   
    data_returned = api.get_data( 
        skip = 0,
        base_url = '',
        limit = 1,
        apikey = '',
        ts = ''
    )     

    os.remove(filename)
    assert data_returned['error'] == True

